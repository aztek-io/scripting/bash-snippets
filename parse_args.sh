#!/usr/bin/env bash
set -e

main(){
    parse_args "$@"

    echo "$t"
}

print_help(){
    EXIT_CODE="${1:-1}"

    cat << EOF >&2
usage:
    $0 [-h] [-t <string>]

example:
    $0 -t target    # prints the target
    $0 -h           # prints help
EOF

    exit "$EXIT_CODE"
}

parse_args(){
    while getopts 'ht:' OPT; do
        case "$OPT" in
            h) print_help 0;;
            t) t="$OPTARG";;
            *) print_help;;
        esac
    done

    [[ -n "$t" ]] || print_help
}

main "$@"
